set PROD;
param cout {PROD} >= 0;
param inf  {PROD} >= 0;
param moy  {PROD} >= 0;
param sup  {PROD} >= 0;

var jour {p in PROD} >= 0;

minimize tot_cout:
	sum {p in PROD} cout[p] * jour[p];

subject to inf_prod:
	sum {p in PROD} (inf[p] * jour[p]) >= 16;

subject to moy_prod:
	sum {p in PROD} (moy[p] * jour[p]) >= 5;

subject to sup_prod:
	sum {p in PROD} (sup[p] * jour[p]) >= 20;

data;

set PROD := usine_a usine_b;
param:    cout inf moy sup :=
usine_a   1000   8   1   2
usine_b   2000   2   1   7;
