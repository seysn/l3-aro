set PROD;
param spec {PROD} >= 0;
param cout {PROD} >= 0;
param minutes_min {PROD} >= 0;
param pers {PROD} >= 0;

var minutes {p in PROD} >= minutes_min[p];
	
maximize tot_spec:
	sum {p in PROD} minutes[p] * spec[p];

subject to cout_limite:
	sum {p in PROD} (minutes[p] * cout[p]) <= 1000000;

subject to pers_limite:
	sum {p in PROD} (minutes[p] * pers[p]) <= 100;
data;

set PROD := tv mag radio;
param:   spec   cout   minutes_min  pers:=
tv    1800000  20000            10     3
mag   1000000  10000             0     1
radio  250000   2000             0     1;