var tv >= 10;
var mag >= 0;
var radio >= 0;

# Spectateurs recoltés
maximize spec : 1800000*tv + 1000000*mag + 250000*radio;

# Coût en €
subject to a : 20000*tv + 10000*mag + 2000*radio <= 1000000;

# Ajouter la contrainte des personnes-semaines
subject to b : 3*mag + tv + radio <= 100;
