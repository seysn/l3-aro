# Exercice 1

set PROD;
param rendement {PROD} >= 0;
param consommation {PROD} >= 0;
param vente_max {PROD} >= 0;
param prix {PROD} >= 0;

var vaches >= 0;
var ares_vegetaux {p in PROD} >= 0;

# Maximiser le profit des éléments sans oublier de soustraire la consommation des vaches
maximize profit:
	200 * vaches + sum {p in PROD} (ares_vegetaux[p] * rendement[p] - vaches * consommation[p]) * prix[p];

# On ne peut utiliser que 200 ares au maximum
subject to ares_limite:
	2 * vaches + sum {p in PROD} ares_vegetaux[p] <= 200;

# Pour chaque vegetaux, on definit la limite de vente
# en prennant en compte la consommation des vaches
subject to vente_limite {p in PROD}:
	ares_vegetaux[p] * rendement[p] - vaches * consommation[p] <= vente_max[p];

# On force une production minimum de vegetaux pour permettre
# de nourrir les vaches
subject to production_minimum {p in PROD}:
	ares_vegetaux[p] * rendement[p] - vaches * consommation[p] >= 0;
	
data;

set PROD := betterave blé mais;
param:    rendement consommation vente_max prix :=
betterave 		  1          0.6        10  100
blé             0.6          0.2        20  120
mais            0.5          0.2        20   90;