/* Couleurs */
typedef enum {ROUGE=0, BLEU=1, VERT=2} tCouleur;
typedef tCouleur tTabCouleurs[MAX_SOMMETS];

void graphe2visu(tGraphe graphe, char *outfile);
void graphe2visuCouleurs(tGraphe graphe, char *outfile, tTabCouleurs tabCouleurs);
