#include <stdio.h>
#include <sys/wait.h>
#include <string.h>
#include <stdlib.h>

#include "graphe.h"
#include "visu.h"

void largeur(tGraphe graphe, tNumeroSommet sommetDepart) {
	tTabCouleurs tabCoul;
	tFileSommets file;
	tNumeroSommet sommet, sommetVoisin;
	int i, j;
	char outfile[80];

	file = fileSommetsAlloue();
	for (i = 0; i < grapheNbSommets(graphe); i++) {
		tabCoul[i] = BLEU;
	}

	tabCoul[sommetDepart] = VERT;
	fileSommetsEnfile(file, sommetDepart);

	i = 0;
	while (!fileSommetsEstVide(file)) {
		sprintf(outfile, "largeur-%d.ps", i++);
		graphe2visuCouleurs(graphe, outfile, tabCoul);
		sommet = fileSommetsDefile(file);
		for (j = 0; j < grapheNbVoisinsSommet(graphe, sommet); j++) {
			sommetVoisin = grapheVoisinSommetNumero(graphe, sommet, j);
			if (tabCoul[sommetVoisin] == BLEU) {
				tabCoul[sommetVoisin] = VERT;
				fileSommetsEnfile(file, sommetVoisin);
				sprintf(outfile, "largeur-%d.ps", i++);
				graphe2visuCouleurs(graphe, outfile, tabCoul);
			}
		}
		tabCoul[sommet] = ROUGE;
	}

	fileSommetsLibere(file);
}


int main(int argc, char **argv) {
	tGraphe graphe;

	if (argc < 3) {
		halt("Usage : %s FichierGraphe SommetDepart\n", argv[0]);
	}
  
	graphe = grapheAlloue();
	grapheChargeFichier(graphe, argv[1]);
	

	largeur(graphe, grapheChercheSommetParNom(graphe, argv[2]));

	grapheLibere(graphe);
	return EXIT_SUCCESS;
}
