#include <stdlib.h>
#include <stdio.h>

#include "graphe.h"
#include "visu.h"

int main(int argc, char *argv[]) {
	tGraphe graphe;
	
	if (argc < 2) {
		halt("Usage : %s FichierGraphe\n", argv[0]);
	}
  
	graphe = grapheAlloue();
  
	grapheChargeFichier(graphe, argv[1]);

	graphe2visu(graphe, "visu.ps");
	grapheLibere(graphe);

	exit(EXIT_SUCCESS);
}
