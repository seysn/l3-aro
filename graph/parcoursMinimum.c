#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "graphe.h"
#include "visu.h"

void plusCourteChaine(tGraphe graphe, tNumeroSommet sommetDepart) {
	tNumeroSommet pred[MAX_SOMMETS];
	int d[MAX_SOMMETS];
	
	tNumeroSommet sommet, sommetVoisin;
	tFileSommets file;
	tTabCouleurs tabCoul;
	tNomSommet nom, nomPred;
	
	int i, j;

	file = fileSommetsAlloue();
	for (i = 0; i < grapheNbSommets(graphe); i++) {
		tabCoul[i] = BLEU;
	}

	d[sommetDepart] = 0;
	pred[sommetDepart] = sommetDepart;
	tabCoul[sommetDepart] = VERT;
	fileSommetsEnfile(file, sommetDepart);

	while (!fileSommetsEstVide(file)) {
		sommet = fileSommetsDefile(file);
		grapheRecupNomSommet(graphe, sommet, nom);
		printf("Defile %s\n", nom);
		for (j = 0; j < grapheNbVoisinsSommet(graphe, sommet); j++) {
			sommetVoisin = grapheVoisinSommetNumero(graphe, sommet, j);
			if (tabCoul[sommetVoisin] != ROUGE) {
				fileSommetsEnfile(file, sommetVoisin);
				tabCoul[sommetVoisin] = VERT;
				d[sommetVoisin] = d[sommet] + 1;
				pred[sommetVoisin] = sommet;
				grapheRecupNomSommet(graphe, sommetVoisin, nom);
				printf(" Voisin %d : %s\n", j + 1, nom);
			}
		}
		tabCoul[sommet] = ROUGE;
	}

	graphe2visuCouleurs(graphe, "testParcoursMinimum.ps", tabCoul);
	
	printf("\nValeurs du tableau d :\n");
	for (i = 0; i < grapheNbSommets(graphe); i++) {
		grapheRecupNomSommet(graphe, i, nom);
		printf("d[%d] (%s) = %d\n", i, nom, d[i]);
	}

	printf("\nValeurs du tableau pred :\n");
	for (i = 0; i < grapheNbSommets(graphe); i++) {
		grapheRecupNomSommet(graphe, i, nom);
		grapheRecupNomSommet(graphe, pred[i], nomPred);
		printf("pred[%d] (%s) = %d (%s)\n", i, nom, pred[i], nomPred);
	}
}

long nbMicroSecondesDepuisDebutHeure(void) {
	struct timeval tv;
	long us;
	gettimeofday(&tv, NULL);
	tv.tv_sec = tv.tv_sec % 3600;
	us = (tv.tv_sec * 1000000) + tv.tv_usec;
	return us;
}

int main(int argc, char **argv) {
	tGraphe graphe;
	long sec;
	
	if (argc < 2) {
		/* halt("Usage : %s FichierGraphe SommetDepart\n", argv[0]); */
		halt("Usage : %s SommetDepart\n", argv[0]);
	}

	sec = nbMicroSecondesDepuisDebutHeure();
	
	graphe = grapheAlloue();
	/*grapheChargeFichier(graphe, argv[1]);*/
	grapheAleatoire(graphe, 10, 0, 0.5);

	plusCourteChaine(graphe, grapheChercheSommetParNom(graphe, argv[1]));

	printf("\nTemps écoulé : %ld\n", nbMicroSecondesDepuisDebutHeure() - sec);
	
	grapheLibere(graphe);
	return EXIT_SUCCESS;
}
