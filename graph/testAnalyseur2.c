#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "graphe.h"
#include "visu.h"

int main(int argc, char *argv[]) {
	tGraphe graphe;
	int i, tmp, maxv = 0;
	tNomSommet nom;
	
	if (argc < 2) {
		halt("Usage : %s FichierGraphe\n", argv[0]);
	}

  
	graphe = grapheAlloue();
  
	grapheChargeFichier(graphe, argv[1]);

	printf("Sommets qui n'ont pas de voisins:\n");
	for (i = 0; i < grapheNbSommets(graphe); i++) {
		tmp = grapheNbVoisinsSommet(graphe, i);
		if (tmp > maxv)
			maxv = tmp;
		if (tmp == 0) {
			grapheRecupNomSommet(graphe, i, nom);
			printf("- %d : %s\n", i, nom);
		}
	}

	printf("Sommets qui ont le plus de voisins (%d voisins) :\n", maxv);
	for (i = 0; i < grapheNbSommets(graphe); i++) {
		if (grapheNbVoisinsSommet(graphe, i) == maxv) {
			grapheRecupNomSommet(graphe, i, nom);
			printf("- %d : %s\n", i, nom);
		}
	}	

	graphe2visu(graphe, "visu.ps");
	grapheLibere(graphe);

	exit(EXIT_SUCCESS);
}
