#include <stdio.h>
#include <sys/wait.h>
#include <string.h>
#include <stdlib.h>

#include "graphe.h"
#include "visu.h"

void graphe2visu(tGraphe graphe, char *outfile) {
	FILE *fic;
	char commande[80];
	char dotfile[80]; /* le fichier dot pour créer le ps */
	int ret;
	tArc arc;
	tNomSommet nom;

	/* on va créer un fichier pour graphviz, dans le fichier "outfile".dot */
	strcpy(dotfile, outfile);
	strcat(dotfile, ".dot");
	fic = fopen(dotfile, "w");
	if (fic == NULL) {
		halt("Ouverture du fichier %s en écriture impossible\n", dotfile);
	}

	if (grapheEstOriente(graphe))
		fprintf(fic, "digraph {\n");
	else
		fprintf(fic, "graph {\n");

	for (int i = 0; i < grapheNbArcs(graphe); i++) {
		arc = grapheRecupArcNumero(graphe, i);
		grapheRecupNomSommet(graphe, arc.orig, nom);
		fprintf(fic, "%s", nom);
		if (grapheEstOriente(graphe))
			fprintf(fic, " -> ");
		else
			fprintf(fic, " -- ");
		grapheRecupNomSommet(graphe, arc.dest, nom);
		fprintf(fic, "%s;\n",nom);
	}
	fprintf(fic, "}");

	/*
	  on parcourt le graphe pour en tirer les informations
	  n´ecessaires pour graphviz.
	  Pour ´ecrire dans le fichier, on utilise fprintf (qui s’utilise
	  comme printf mais on mettant en plus fic comme premier param`etre).
	  Ex :
	  fprintf(fic, "graph {\n");
	  ou
	  fprintf(fic, " %s -> %s\n", origine, destination);
	*/
	fclose(fic);
	sprintf(commande, "dot -Tps %s -o %s", dotfile, outfile);
	ret = system(commande);
	
	if (WEXITSTATUS(ret)) {
		halt("La commande suivante a échoué\n%s\n", commande);
	}
}


void graphe2visuCouleurs(tGraphe graphe, char *outfile, tTabCouleurs tabCouleurs) {
	FILE *fic;
	char commande[80];
	char dotfile[80]; /* le fichier dot pour créer le ps */
	int ret, i;
	tArc arc;
	tNomSommet nom;

	/* on va créer un fichier pour graphviz, dans le fichier "outfile".dot */
	strcpy(dotfile, outfile);
	strcat(dotfile, ".dot");
	fic = fopen(dotfile, "w");
	if (fic == NULL) {
		halt("Ouverture du fichier %s en écriture impossible\n", dotfile);
	}

	if (grapheEstOriente(graphe))
		fprintf(fic, "digraph {\n");
	else
		fprintf(fic, "graph {\n");

	for (i = 0; i < grapheNbSommets(graphe); i++) {
		grapheRecupNomSommet(graphe, i, nom);
		fprintf(fic, "%s [color=", nom);
		if (tabCouleurs[i] == ROUGE)
			fprintf(fic, "red];\n");
		else if (tabCouleurs[i] == VERT)
			fprintf(fic, "green];\n");
		else if (tabCouleurs[i] == BLEU)
			fprintf(fic, "blue];\n");
	}
	
	for (i = 0; i < grapheNbArcs(graphe); i++) {
		arc = grapheRecupArcNumero(graphe, i);
		grapheRecupNomSommet(graphe, arc.orig, nom);
		fprintf(fic, "%s", nom);
		if (grapheEstOriente(graphe))
			fprintf(fic, " -> ");
		else
			fprintf(fic, " -- ");
		grapheRecupNomSommet(graphe, arc.dest, nom);
		fprintf(fic, "%s;\n",nom);
	}
	fprintf(fic, "}");

	/*
	  on parcourt le graphe pour en tirer les informations
	  n´ecessaires pour graphviz.
	  Pour ´ecrire dans le fichier, on utilise fprintf (qui s’utilise
	  comme printf mais on mettant en plus fic comme premier param`etre).
	  Ex :
	  fprintf(fic, "graph {\n");
	  ou
	  fprintf(fic, " %s -> %s\n", origine, destination);
	*/
	fclose(fic);
	sprintf(commande, "dot -Tps %s -o %s", dotfile, outfile);
	ret = system(commande);
	
	if (WEXITSTATUS(ret)) {
		halt("La commande suivante a échoué\n%s\n", commande);
	}	
}
